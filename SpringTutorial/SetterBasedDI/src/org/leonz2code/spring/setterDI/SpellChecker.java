package org.leonz2code.spring.setterDI;

public class SpellChecker {
	   public SpellChecker(){
	      System.out.println("Inside SpellChecker constructor." );
	   }
	   public void checkSpelling() {
	      System.out.println("Inside checkSpelling." );
	   }
	}
