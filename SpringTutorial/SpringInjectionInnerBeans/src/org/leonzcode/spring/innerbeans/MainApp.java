package org.leonzcode.spring.innerbeans;


//SpringInjectionInnerBeans Project Name

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainApp {
	public static void main(String[] args) {
		ApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");
		TextEditor te = (TextEditor) context.getBean("textEditor");
		te.spellCheck();
	}
	/***************************************************************************************************
As you know Java inner classes are defined within the scope of other classes, similarly, inner beans are beans that are defined
within the scope of another bean. Thus, a <bean/> element inside the <property/> or <constructor-arg/>
elements is called inner bean and it is shown below.

<?xml version = "1.0" encoding = "UTF-8"?>

<beans xmlns = "http://www.springframework.org/schema/beans"
xmlns:xsi = "http://www.w3.org/2001/XMLSchema-instance"
xsi:schemaLocation = "http://www.springframework.org/schema/beans
http://www.springframework.org/schema/�/spring-beans-3.0.xsd">

<bean id = "outerBean" class = "...">
<property name = "target">
<bean id = "innerBean" class = "..."/>
</property>
</bean>

</beans>

	 ****************************************************************************************************/
}

