package org.leonz2code.spring.belicy;

import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainApp {
   public static void main(String[] args) {
      AbstractApplicationContext context = new ClassPathXmlApplicationContext("Beans.xml");

      HelloWorld obj = (HelloWorld) context.getBean("leo_belocy");
      obj.getMessage();
      context.registerShutdownHook(); //This will ensure a graceful shutdown and call the relevant destroy methods.
   }
}
