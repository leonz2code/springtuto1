package org.leonz2code.di;

public class Main {		

		public static void main(String[] args) {
			
/***************************************************************************************
  hasta aqui todo normal CLASE  "ServicioImpresion" METODO "imprimir()"
  tomamos la clase ServicioImpresion y ejecutamos el metodo Imprimir
  lo mas normal seria que nuestra aplicacion divida sus responsabilidades
 
 		Output:
		enviando el documento a imprimir
		imprimiendo el documento en formato pdf
 
 
 ***************************************************************************************/						
	
	//		ServicioImpresion obj = new ServicioImpresion();			
	//		obj.imprimir();			

				
/**************************************************************************************
	Aqui el resultado es el mismo pero hemos dividido mejor las dependencias
	CLASE ServicioImpresionDI,ServicioPDF,ServicioEnvio
	METODO ServicioImpresionDI -> imprimir();

		Output:
		enviando el documento a imprimir
		imprimiendo el documento en formato pdf
***************************************************************************************/			
	//		ServicioImpresionDI objDI = new ServicioImpresionDI();			
	//		objDI.imprimir();				
			
/**************************************************************************************			
							DEPENDENCY INJECTION
***************************************************************************************/			
			
			
/*Se puede realizar la misma operaci�n inyectando las dependencias al ServicioImpresi�n y que no sea �l el que tenga que definirlas en el constructor.
 * Este parece en principio un cambio sin importancia, el c�digo quedar�a:
 *  CLASE "ServicioImpresionCDI"       METODO  ServicioImpresionCDI -> imprimir();
 * */			
													// SE CREAN OBJETOS DE LAS CLASES Y SE PASAN COMO ARGUMENTO AL CONSTRUCTOR al mismo tiempo
	//	ServicioImpresionCDI miServicio=new ServicioImpresionCDI(new ServicioEnvio(),new ServicioPDF());
	//	miServicio.imprimir();			//ya no es el propio servicio el responsable de definir sus dependencias sino que lo es el programa principal (MAIN)
			

	/*El resultado sigue siendo el mismo. Acabamos de inyectar las dependencias en el servicio desde nuestro programa main . �Qu� ventajas aporta esto? 
 La realidad es que en principio parece que ninguna . Pero hay algo que ha cambiado ya no es el propio servicio el responsable de definir sus dependencias
sino que lo es el programa principal. Esto abre las puertas a la extensibilidad. Es decir �tenemos nosotros siempre que inyectar las mismas
dependencias al servicio?. La respuesta parece obvia� claro que s� est�n ya definidas. Sin embargo la respuesta es NO , nosotros podemos cambiar el
tipo de dependencia que inyectamos , simplemente extendiendo una de nuestras clases y cambiando el comportamiento, vamos a verlo.*/	
		
		// CLASS "ServicioEnvioAspecto" introduccion a AOP
			
				ServicioImpresionCDI miServicioA=new ServicioImpresionCDI(new ServicioEnvioAspecto(),new ServicioPDF());
				miServicioA.imprimir();
		
				/*
				Acabamos de modificar el comportamiento de nuestro programa de forma significativa gracias al uso del concepto de inyecci�n de dependencia.
				
				La inyecci�n de dependencia nos permite inyectar otras clases y a�adir funcionalidad transversal a medida. Este patr�n de dise�o es el que abre la
				puerta a frameworks como Spring utilizando el concepto de inyecci�n de dependencia de una forma m�s avanzada. En estos framework los aspectos que se
				a�aden a nuestras clases son m�ltiples y la complejidad alta.
				
				La importancia del patr�n de inyecci�n de dependencia es hoy clave en la mayor�a de los frameworks.
			*/
			
		}

}
