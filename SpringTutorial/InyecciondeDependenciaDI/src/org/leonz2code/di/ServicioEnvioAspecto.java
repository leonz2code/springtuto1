package org.leonz2code.di;

public class ServicioEnvioAspecto extends ServicioEnvio {
	@Override
	public void enviar() {
		System.out.println("haciendo log del correo que vamos a enviar");
		super.enviar();  // INVOCA AL METODO DE LA CLASE PADRE SE EJECUTARAN AMBOS METODOS CON EL MISMO NOMBRE
	}
	/*
	 Acabamos de crear una clase que extiende ServicioEnvio y a�ade una funcionalidad adicional de log que hace un �log� del correo que enviamos . Ahora es
tan sencillo como decirle al programa principal que cuando inyecte la dependencia no inyecte el ServicioEnvio sino el ServicioEnvioAspecto de esta
forma habremos cambiado el comportamiento de forma considerable.*/
	
}
