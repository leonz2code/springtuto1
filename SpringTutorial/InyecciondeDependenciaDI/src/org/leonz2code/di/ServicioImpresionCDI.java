package org.leonz2code.di;

public class ServicioImpresionCDI {
	ServicioEnvio servicioA;
	ServicioPDF servicioB;
	
	public ServicioImpresionCDI(ServicioEnvio servicioA,ServicioPDF servicioB) {
		this.servicioA= servicioA;
		this.servicioB= servicioB;
	}
	
	public void imprimir() {
		servicioA.enviar();
		servicioB.pdf();
	}
}
