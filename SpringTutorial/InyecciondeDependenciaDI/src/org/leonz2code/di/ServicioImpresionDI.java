package org.leonz2code.di;

public class ServicioImpresionDI {
	ServicioEnvio servicioA;
	ServicioPDF servicioB;

	public ServicioImpresionDI() {
		this.servicioA= new ServicioEnvio();
		this.servicioB= new ServicioPDF();
	}
	
	public void imprimir() {
		servicioA.enviar();
		servicioB.pdf();
		}
}