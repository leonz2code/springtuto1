package org.leonz2code.spring.example1;

import org.springframework.beans.factory.InitializingBean; 
import org.springframework.beans.factory.xml.XmlBeanFactory; 
import org.springframework.core.io.ClassPathResource;  

public class MainApp { 
   public static void main(String[] args) { 
	   
	   //---------------DEPRECADA DESDE 2011 EN SU LUGAR USAR  ApplicationContext context= new ClassPathXmlApplicationContext---------------
      XmlBeanFactory factory = new XmlBeanFactory (new ClassPathResource("Beans.xml")); 
      HelloWorld obj = (HelloWorld) factory.getBean("helloWorld");    
      obj.getMessage();    
   }
}   